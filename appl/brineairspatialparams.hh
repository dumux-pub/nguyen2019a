// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the water-air problem.
 */
#ifndef DUMUX_BRINE_AIR_SPATIAL_PARAMS_HH
#define DUMUX_BRINE_AIR_SPATIAL_PARAMS_HH

#include <dumux/io/ploteffectivediffusivitymodel.hh>
#include <dumux/io/plotmateriallaw.hh>
#include <dumux/io/plotthermalconductivitymodel.hh>
#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{
/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Definition of the spatial parameters for the water-air problem
 */
template<class FVGridGeometry, class Scalar>
class BrineAirSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                         BrineAirSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, BrineAirSpatialParams<FVGridGeometry, Scalar>>;

    static constexpr int dimWorld = GridView::dimensionworld;

    using EffectiveLaw = RegularizedBrooksCorey<Scalar>;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! Export the type used for the permeability
    using PermeabilityType = Scalar;
    //! Export the material law type used
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;
    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    BrineAirSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
    {
        layerBottom_ = 22.0;

        // intrinsic permeabilities
        fineK_ = getParam<Scalar>("Component.SolidPermeability");;
        coarseK_ = getParam<Scalar>("Component.SolidPermeability");;

//         permeabtility = (pow(((1-So)/(1-Sor)),2.0))*(1-(pow(((So-Sor)/(1-Sor)),((2+lambda)/lambda))))
        
        // porosities
        finePorosity_ = getParam<Scalar>("Component.SolidPorosity");
        coarsePorosity_ = getParam<Scalar>("Component.SolidPorosity");

        // residual saturations
        fineMaterialParams_.setSwr(getParam<Scalar>("Component.LiquidPhaseResidualSaturation"));
        fineMaterialParams_.setSnr(getParam<Scalar>("Component.GasPhaseResidualSaturation"));
        coarseMaterialParams_.setSwr(getParam<Scalar>("Component.LiquidPhaseResidualSaturation"));
        coarseMaterialParams_.setSnr(getParam<Scalar>("Component.GasPhaseResidualSaturation"));

        // parameters for the Brooks-Corey law
        fineMaterialParams_.setPe(getParam<Scalar>("BrooksCoreyModel.CapillaryPressureStrength"));
        coarseMaterialParams_.setPe(getParam<Scalar>("BrooksCoreyModel.CapillaryPressureStrength"));
        fineMaterialParams_.setLambda(getParam<Scalar>("BrooksCoreyModel.BrooksCoreyLambda"));
        coarseMaterialParams_.setLambda(getParam<Scalar>("BrooksCoreyModel.BrooksCoreyLambda"));

        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");
    }

    /*!
     * \brief This is called from the problem and creates a gnuplot output
     *        of e.g the pc-Sw curve
     */
    void plotMaterialLaw()
    {
        PlotMaterialLaw<Scalar, MaterialLaw> plotMaterialLaw;
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);
        plotMaterialLaw.addpcswcurve(gnuplot, fineMaterialParams_, 0.2, 1.0, "fine", "w lp");
        plotMaterialLaw.addpcswcurve(gnuplot, coarseMaterialParams_, 0.2, 1.0, "coarse", "w l");
        gnuplot.setOption("set xrange [0:1]");
        gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");
    }

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (isFineMaterial_(globalPos))
            return fineK_;
        return coarseK_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (isFineMaterial_(globalPos))
            return finePorosity_;
        else
            return coarsePorosity_;
    }


    /*!
     * \brief Returns the parameter object for the capillary-pressure/
     *        saturation material law
     *
     * \param globalPos The global position
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        if (isFineMaterial_(globalPos))
            return fineMaterialParams_;
        else
            return coarseMaterialParams_;
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The position of the center of the element
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }
    
    /*!
     * \brief Returns the user-defined solid heat capacity.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \param solidState The solid state
     * \return The solid heat capacity
     */
    template <class ElementSolution, class SolidState>
    Scalar solidHeatCapacity(const Element& element,
                             const SubControlVolume& scv,
                             const ElementSolution& elemSol,
                             const SolidState& solidState) const
    {
        return getParam<Scalar>("Component.SolidHeatCapacity");
    }
    
    /*!
     * \brief Returns the user-defined solid heat capacity.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \param solidState The solid state
     * \return The solid heat capacity
     */
    template <class ElementSolution, class SolidState>
    Scalar solidThermalConductivity(const Element& element,
                             const SubControlVolume& scv,
                             const ElementSolution& elemSol,
                             const SolidState& solidState) const
    {
        return getParam<Scalar>("Component.SolidThermalConductivity");
    }
    
    /*!
     * \brief Returns the user-defined solid heat capacity.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \param solidState The solid state
     * \return The solid heat capacity
     */
    template <class ElementSolution, class SolidState>
    Scalar solidDensity(const Element& element,
                             const SubControlVolume& scv,
                             const ElementSolution& elemSol,
                             const SolidState& solidState) const
    {
        return getParam<Scalar>("Component.SolidDensity");
    }

private:
    bool isFineMaterial_(const GlobalPosition &globalPos) const
    { return globalPos[dimWorld-1] > layerBottom_; }

    Scalar fineK_;
    Scalar coarseK_;
    Scalar layerBottom_;
    Scalar saturationG_;

    Scalar finePorosity_;
    Scalar coarsePorosity_;

    MaterialLawParams fineMaterialParams_;
    MaterialLawParams coarseMaterialParams_;

    bool plotFluidMatrixInteractions_;
};

}

#endif
